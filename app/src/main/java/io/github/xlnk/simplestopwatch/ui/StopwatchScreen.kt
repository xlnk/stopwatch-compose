package io.github.xlnk.simplestopwatch

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.bignerdranch.android.materia2test.ui.theme.StopwatchTheme
import io.github.xlnk.simplestopwatch.data.AmountOfTimeProvider
import io.github.xlnk.simplestopwatch.data.Lap
import io.github.xlnk.simplestopwatch.data.StopwatchStatus
import io.github.xlnk.simplestopwatch.ui.AmountOfTime
import io.github.xlnk.simplestopwatch.ui.LapList
import io.github.xlnk.simplestopwatch.ui.NoNotificationPermissionWarning
import io.github.xlnk.simplestopwatch.ui.preview.PreviewAmountOfTimeProvider
import io.github.xlnk.simplestopwatch.ui.preview.PreviewData

private val modifierButtons = Modifier.widthIn(min = 104.dp)

private const val TAG = "StopwatchScreen"

@Composable
fun StopwatchScreen(
    status: StopwatchStatus,
    amountOfTimeProvider: AmountOfTimeProvider,
    lapList: List<Lap>,
    onLapOrReset: () -> Unit,
    onStartPause: () -> Unit,
    modifier: Modifier = Modifier,
    noNotificationPermission: Boolean = false,
    onOpenPermissionSettings: (() -> Unit)? = null,
    blocked: Boolean = false,
) {
    BoxWithConstraints(modifier = modifier.fillMaxSize()) {
        val maxHeight = this.maxHeight

        Column(
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxSize()
        ) {
            if (noNotificationPermission) {
                NoNotificationPermissionWarning(onOpenPermissionSettings, Modifier.padding(8.dp))
            } else {
                Spacer(Modifier.size(Dp.Hairline))
            }

            AmountOfTime(
                amountOfTimeProvider = amountOfTimeProvider,
            )

            if (lapList.isNotEmpty()) {
                LapList(
                    lapList = lapList,
                    modifier = Modifier
                        .padding(horizontal = 32.dp)
                        .heightIn(max = maxHeight * 0.5f),
                )
            }

            val resetVisible = status != StopwatchStatus.CLEARED

            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly,
                modifier = Modifier
                    .heightIn(min = 60.dp)
                    .fillMaxWidth()
            ) {
                if (resetVisible) {
                    Button(
                        onClick = onLapOrReset,
                        modifier = modifierButtons,
                        enabled = !blocked,
                    ) {
                        Text(stringResource(if (status == StopwatchStatus.RUNNING) R.string.lap else R.string.reset))
                    }
                }

                val buttonTextId = when (status) {
                    StopwatchStatus.PAUSED -> R.string.resume
                    StopwatchStatus.RUNNING -> R.string.stop
                    StopwatchStatus.CLEARED -> R.string.start
                }
                val buttonColor = if (status == StopwatchStatus.RUNNING) {
                    MaterialTheme.colors.secondaryVariant
                } else {
                    MaterialTheme.colors.secondary
                }

                Button(
                    onClick = onStartPause,
                    modifier = modifierButtons,
                    colors = ButtonDefaults.buttonColors(buttonColor),
                    enabled = !blocked,
                ) {
                    Text(stringResource(buttonTextId))
                }
            }
            Spacer(Modifier.size(Dp.Hairline))
        }
    }

}

@Preview
@Composable
fun StopwatchScreenPreviewCleared() {
    StopwatchTheme() {
        StopwatchScreen(
            StopwatchStatus.CLEARED,
            PreviewAmountOfTimeProvider(),
            PreviewData.lapList,
            {},
            {},
        )
    }
}

@Preview
@Composable
fun StopwatchScreenPreviewRunning() {
    StopwatchTheme() {
        StopwatchScreen(
            StopwatchStatus.RUNNING,
            PreviewAmountOfTimeProvider(),
            PreviewData.lapList,
            {},
            {},
        )
    }
}

@Preview
@Composable
fun StopwatchScreenPreviewPaused() {
    StopwatchTheme() {
        StopwatchScreen(
            StopwatchStatus.PAUSED,
            PreviewAmountOfTimeProvider(),
            PreviewData.lapList,
            {},
            {},
        )
    }
}

@Preview
@Composable
fun StopwatchScreenPreviewRunningBig() {
    StopwatchTheme() {
        StopwatchScreen(
            StopwatchStatus.RUNNING,
            PreviewAmountOfTimeProvider(PreviewData.lapListBig.sumOf { it.lapTimeMs } + 16550),
            PreviewData.lapListBig,
            {},
            {},
        )
    }
}

@Composable
fun StopwatchScreenPreviewRunningEmpty() {
    StopwatchTheme() {
        StopwatchScreen(
            StopwatchStatus.PAUSED,
            PreviewAmountOfTimeProvider(PreviewData.lapListBig.sumOf { it.lapTimeMs } + 16550),
            emptyList(),
            {},
            {},
        )
    }
}

@Composable
fun StopwatchScreenPreviewNoNotification() {
    StopwatchTheme() {
        StopwatchScreen(
            StopwatchStatus.PAUSED,
            PreviewAmountOfTimeProvider(PreviewData.lapListBig.sumOf { it.lapTimeMs } + 16550),
            emptyList(),
            {},
            {},
            noNotificationPermission = true,
            onOpenPermissionSettings = {}
        )
    }
}