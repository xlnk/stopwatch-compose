package io.github.xlnk.simplestopwatch.data.settings

import io.github.xlnk.simplestopwatch.data.Lap
import kotlinx.coroutines.flow.StateFlow

interface SettingsRepository {
    fun setLapList(laps: List<Lap>)
    fun setPeriod(value: Long)
    val settings: StateFlow<StopwatchSettings>
    fun reset()
}