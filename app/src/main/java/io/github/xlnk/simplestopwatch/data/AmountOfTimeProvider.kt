package io.github.xlnk.simplestopwatch.data

import kotlinx.coroutines.flow.StateFlow

interface AmountOfTimeProvider {
    val amountOfTimeMs: StateFlow<Long>
    val amountOfTimeText: StateFlow<String>
}