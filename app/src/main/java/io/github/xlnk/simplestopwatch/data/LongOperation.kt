package io.github.xlnk.simplestopwatch.data

sealed class LongOperation {
    object Loading : LongOperation()
    object Ready : LongOperation()
    data class Error(val throwable: Throwable): LongOperation()
}
