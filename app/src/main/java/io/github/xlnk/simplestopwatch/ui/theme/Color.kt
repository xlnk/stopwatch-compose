package com.bignerdranch.android.materia2test.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val DeepPurple700 = Color(0xFF512DA8)
val DeepPurple800 = Color(0xFF4527A0)
val DeepPurple900 = Color(0xFF311B92)
val Pink700 = Color(0xFFC2185B)
val Pink900 = Color(0xFF880E4F)
val Gray400 = Color(0xFFBDBDBD)
val Gray700 = Color(0xFF616161)
val BlueGray400 = Color(0xFF78909C)
val BlueGray700 = Color(0xFF455A64)
