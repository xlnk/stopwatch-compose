package io.github.xlnk.simplestopwatch.ui

import androidx.compose.foundation.layout.size
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import io.github.xlnk.simplestopwatch.BuildConfig

@Composable
fun InitCrashlyticsButton(modifier: Modifier = Modifier) {
    if (BuildConfig.DEBUG) {
        Button(modifier = modifier.size(80.dp, 32.dp), onClick = {
            throw RuntimeException("Test Crash for Crashlytics")
        }) {
            Text("Test_Crash")
        }
    }
}