package io.github.xlnk.simplestopwatch.data.settings

import io.github.xlnk.simplestopwatch.data.Lap
import io.github.xlnk.simplestopwatch.data.LongOperation

data class StopwatchSettings(
    val period: Long = 0L,
    val loadingStatus: LongOperation = LongOperation.Loading,
    val lapList: List<Lap> = emptyList(),
)