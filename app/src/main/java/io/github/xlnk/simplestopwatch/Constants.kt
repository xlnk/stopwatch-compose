package io.github.xlnk.simplestopwatch

object Constants {
    const val SERVICE_NOTIFICATION_ID = 2
    const val CHANNEL_DEFAULT = "channel_default"
    const val SETTINGS_NAME = "settings"
    const val MILLISECOND_TIMEOUT = 100L
}