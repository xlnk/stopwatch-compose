package io.github.xlnk.simplestopwatch.data

enum class StopwatchStatus {
    CLEARED, RUNNING, PAUSED
}