package io.github.xlnk.simplestopwatch.data

import androidx.compose.runtime.Immutable

@Immutable
data class Lap(
    val lapTimeMs: Long,
    val overallTimeMs: Long,
) {
    val lapTimeText by lazy(LazyThreadSafetyMode.PUBLICATION) {
        FormatUtil.formatPeriodMs(lapTimeMs)
    }

    val overallTimeText by lazy(LazyThreadSafetyMode.PUBLICATION) {
        FormatUtil.formatPeriodMs(overallTimeMs)
    }
}