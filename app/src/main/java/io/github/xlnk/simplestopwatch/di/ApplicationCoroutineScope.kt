package io.github.xlnk.simplestopwatch.di

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationCoroutineScope()
