package io.github.xlnk.simplestopwatch.ui.preview

import androidx.compose.ui.tooling.preview.datasource.CollectionPreviewParameterProvider
import io.github.xlnk.simplestopwatch.data.Lap



object PreviewData {
    val lap1 = Lap(11_110, 11_110)
    val lap2 = Lap(22_220, 33_330)
    val lap3 = Lap(33_000, 66_330)

    val lapList = listOf(lap1, lap2, lap3)
    val lapListBig: List<Lap> by lazy {
        var overallTimeMs = 0L
        val result = mutableListOf<Lap>()
        for (i in 0 until 42) {
            val lap = lapList[i % lapList.size]
            overallTimeMs += lap.lapTimeMs
            result.add(lap.copy(overallTimeMs = overallTimeMs))
        }
        result
    }

    class LapPreviewProvider : CollectionPreviewParameterProvider<Lap>(listOf(lap1, lap2, lap3))

    class LapListPreviewProvider : CollectionPreviewParameterProvider<List<Lap>>(listOf(listOf(lap1, lap2, lap3)))
}