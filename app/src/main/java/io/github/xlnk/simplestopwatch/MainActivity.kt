package io.github.xlnk.simplestopwatch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.content.ContextCompat
import dagger.hilt.android.AndroidEntryPoint
import io.github.xlnk.simplestopwatch.data.StopwatchLogic
import io.github.xlnk.simplestopwatch.data.StopwatchStatus
import io.github.xlnk.simplestopwatch.ui.InitCrashlyticsButton
import io.github.xlnk.simplestopwatch.ui.StopwatchApp
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @Inject
    internal lateinit var stopwatchLogic: StopwatchLogic

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_SimpleStopwatch)

        super.onCreate(savedInstanceState)

        setContent {
            StopwatchApp(stopwatchLogic)
        }
    }

    override fun onStart() {
        super.onStart()

        application.stopService(StopwatchForegroundService.newIntent(application))
        stopwatchLogic.setModeMs()
    }

    override fun onStop() {
        super.onStop()

        when (stopwatchLogic.status.value) {
            StopwatchStatus.PAUSED, StopwatchStatus.RUNNING -> {
                ContextCompat.startForegroundService(
                    application,
                    StopwatchForegroundService.newIntent(application)
                )
                stopwatchLogic.setModeSecond()
            }

            StopwatchStatus.CLEARED -> Unit
        }
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}