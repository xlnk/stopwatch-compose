package io.github.xlnk.simplestopwatch.ui

import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import io.github.xlnk.simplestopwatch.R

@Composable
fun PermissionNotificationExplanationDialog(
    onRequestPermission: () -> Unit,
    onRejectPermission: () -> Unit,
    modifier: Modifier = Modifier,
) {
    AlertDialog(
        modifier = modifier,
        onDismissRequest = onRejectPermission,
        confirmButton = {
            TextButton(
                onClick = onRequestPermission,
            ) {
                Text(stringResource(R.string.request_permission))
            }
        },
        dismissButton = {
            TextButton(
                onClick = onRejectPermission,
            ) {
                Text(stringResource(R.string.refuse_text))
            }
        },
        title = { Text(stringResource(R.string.require_permission)) },
        text = { Text(stringResource(R.string.permission_notification_explanation))}
    )
}