package io.github.xlnk.simplestopwatch.data

import io.github.xlnk.simplestopwatch.Constants
import io.github.xlnk.simplestopwatch.data.settings.SettingsRepository
import io.github.xlnk.simplestopwatch.di.ApplicationCoroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StopwatchLogic @Inject constructor(
    private val settingsRepository: SettingsRepository,
    @ApplicationCoroutineScope
    private val externalScope: CoroutineScope,
) : AmountOfTimeProvider {
    init {
        externalScope.launch {
            val settings = settingsRepository.settings.first { it.loadingStatus != LongOperation.Loading }

            _amountOfTimeMs.value = settings.period
            _status.value = if (settings.period > 0) StopwatchStatus.PAUSED else StopwatchStatus.CLEARED
            _loadingStatus.value = settings.loadingStatus
            _laps.value = settings.lapList
        }
    }

    private val _loadingStatus = MutableStateFlow<LongOperation>(LongOperation.Loading)
    val loadingStatus = _loadingStatus.asStateFlow()

    private val _amountOfTimeMs = MutableStateFlow(0L)
    override val amountOfTimeMs = _amountOfTimeMs.asStateFlow()

    private val _status = MutableStateFlow(StopwatchStatus.CLEARED)
    val status = _status.asStateFlow()

    override val amountOfTimeText: StateFlow<String> = _amountOfTimeMs.map {
        FormatUtil.formatPeriodMs(it)
    }.stateIn(externalScope, SharingStarted.WhileSubscribed(5000), "")

    val amountOfTimeSecondsText: StateFlow<String> = _amountOfTimeMs.map {
        FormatUtil.formatPeriodSeconds(it)
    }.stateIn(externalScope, SharingStarted.WhileSubscribed(1000), "")

    private var job: Job? = null
    private var startTimeMs: Long = 0
    private var modeSeconds = false
    private var forceStop = false

    private val _laps = MutableStateFlow(listOf<Lap>())
    val lapList = _laps.asStateFlow()

    private fun runInfinityLoop(): Job {
        return externalScope.launch {
            while (true) {
                val currentTimeMs = System.currentTimeMillis()
                if (!forceStop) {
                    _amountOfTimeMs.value = currentTimeMs - startTimeMs
                }

                if (modeSeconds) {
                    delay(currentTimeMs % 1000)
                } else {
                    delay(Constants.MILLISECOND_TIMEOUT)
                }
            }
        }
    }

    fun start(): Boolean {
        if (_loadingStatus.value != LongOperation.Ready) return false

        return when (_status.value) {
            StopwatchStatus.RUNNING -> false
            StopwatchStatus.CLEARED, StopwatchStatus.PAUSED -> {
                startTimeMs = System.currentTimeMillis() - _amountOfTimeMs.value
                forceStop = false
                _status.value = StopwatchStatus.RUNNING
                job = runInfinityLoop()
                true
            }
        }
    }

    fun pause(): Boolean {
        val currentTimeMs = System.currentTimeMillis()
        forceStop = true
        job?.cancel()
        return when (_status.value) {
            StopwatchStatus.PAUSED, StopwatchStatus.CLEARED -> false
            StopwatchStatus.RUNNING -> {
                val period = currentTimeMs - startTimeMs
                _amountOfTimeMs.value = period
                startTimeMs = 0
                _status.value = StopwatchStatus.PAUSED
                settingsRepository.setPeriod(period)
                true
            }
        }
    }

    fun startPause(): Boolean = when (_status.value) {
        StopwatchStatus.CLEARED, StopwatchStatus.PAUSED -> start()
        StopwatchStatus.RUNNING -> pause()
    }

    fun reset(): Boolean {
        if (_loadingStatus.value != LongOperation.Ready) return false

        forceStop = true
        job?.cancel()
        _laps.value = emptyList()
        _status.value = StopwatchStatus.CLEARED
        _amountOfTimeMs.value = 0
        startTimeMs = 0
        settingsRepository.reset()
        return true
    }

    fun setModeSecond() {
        modeSeconds = true
    }

    fun setModeMs() {
        modeSeconds = false
        if (_status.value == StopwatchStatus.RUNNING) {
            job?.cancel()
            job = runInfinityLoop()
        }
    }

    fun nextLap() {
        if (_loadingStatus.value != LongOperation.Ready) return
        if (_status.value != StopwatchStatus.RUNNING) return

        val currentTimeMs = System.currentTimeMillis()
        val overallTimeMs = currentTimeMs - startTimeMs

        _laps.update {
            val prevOverallTime = it.lastOrNull()?.overallTimeMs ?: 0
            val lapTimeMs = overallTimeMs - prevOverallTime
            val lap = Lap(lapTimeMs, overallTimeMs)

            val result = listOf(lap).plus(it)
            settingsRepository.setLapList(result)
            result
        }
    }

    fun lapOrReset() {
        when (_status.value) {
            StopwatchStatus.RUNNING -> nextLap()
            StopwatchStatus.CLEARED -> Unit
            StopwatchStatus.PAUSED -> reset()
        }
    }
}