package io.github.xlnk.simplestopwatch.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.bignerdranch.android.materia2test.ui.theme.StopwatchTheme
import io.github.xlnk.simplestopwatch.R

@Composable
fun SeriousErrorScreen(error: Throwable, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = stringResource(R.string.serious_error),
            color = MaterialTheme.colors.error,
            style = MaterialTheme.typography.h3,
        )
        Text(
            text = UiUtils.formatError(error),
            color = MaterialTheme.colors.error,
            style = MaterialTheme.typography.h5,
        )
    }
}

@Preview
@Composable
fun SeriousErrorScreenPreview() {
    StopwatchTheme() {
        SeriousErrorScreen(NullPointerException())
    }
}