package io.github.xlnk.simplestopwatch

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import dagger.hilt.android.AndroidEntryPoint
import io.github.xlnk.simplestopwatch.data.StopwatchLogic
import javax.inject.Inject

@AndroidEntryPoint
class CommandReceiver : BroadcastReceiver() {
    @Inject
    internal lateinit var stopwatchLogic: StopwatchLogic

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            ACTION_START_PAUSE -> {
                stopwatchLogic.startPause()
            }
            ACTION_LAP_OR_RESET -> {
                stopwatchLogic.lapOrReset()
            }
        }
    }

    companion object {
        private const val ACTION_START_PAUSE = "io.github.xlnk.simplestopwatch.ACTION_START_PAUSE"
        private const val ACTION_LAP_OR_RESET = "io.github.xlnk.simplestopwatch.ACTION_LAP_OR_RESET"

        @JvmStatic
        fun getStartPauseIntent(context: Context): Intent {
            return Intent(context, CommandReceiver::class.java).apply {
                this.action = ACTION_START_PAUSE
            }
        }

        @JvmStatic
        fun getLapOrResetIntent(context: Context): Intent {
            return Intent(context, CommandReceiver::class.java).apply {
                this.action = ACTION_LAP_OR_RESET
            }
        }
    }
}