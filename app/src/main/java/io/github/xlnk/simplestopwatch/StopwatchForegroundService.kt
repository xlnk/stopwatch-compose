package io.github.xlnk.simplestopwatch

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import dagger.hilt.android.AndroidEntryPoint
import io.github.xlnk.simplestopwatch.Constants.SERVICE_NOTIFICATION_ID
import io.github.xlnk.simplestopwatch.data.StopwatchLogic
import io.github.xlnk.simplestopwatch.data.StopwatchStatus
import io.github.xlnk.simplestopwatch.ui.UiUtils
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class StopwatchForegroundService : LifecycleService() {

    private var notificationManager: NotificationManager? = null
    private lateinit var activityShowIntent: Intent
    private lateinit var activityShowPendingIntent: PendingIntent
    private lateinit var startPauseStopPendingIntent: PendingIntent
    private lateinit var lapOrResetPendingIntent: PendingIntent

    @Inject
    internal lateinit var stopwatchLogic: StopwatchLogic

    override fun onCreate() {
        super.onCreate()

        notificationManager = ContextCompat.getSystemService(applicationContext, NotificationManager::class.java)

        activityShowIntent = MainActivity.newIntent(applicationContext)
        activityShowPendingIntent = PendingIntent.getActivity(applicationContext, 0, activityShowIntent, PendingIntent.FLAG_IMMUTABLE)
        startPauseStopPendingIntent = PendingIntent.getBroadcast(applicationContext, 0, CommandReceiver.getStartPauseIntent(applicationContext), PendingIntent.FLAG_IMMUTABLE)
        lapOrResetPendingIntent = PendingIntent.getBroadcast(applicationContext, 0, CommandReceiver.getLapOrResetIntent(applicationContext), PendingIntent.FLAG_IMMUTABLE)
    }

    override fun onBind(intent: Intent): IBinder? = super.onBind(intent)

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForeground(SERVICE_NOTIFICATION_ID, createServiceNotification(stopwatchLogic.amountOfTimeSecondsText.value, stopwatchLogic.status.value))

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                stopwatchLogic.status.collect { status ->
                    if (status == StopwatchStatus.CLEARED) {
                        stopSelf()
                    }
                }
            }
        }

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                combine(stopwatchLogic.amountOfTimeSecondsText, stopwatchLogic.status, stopwatchLogic.lapList) { amountOfTimeSecondsText, status, lapList ->
                    UiUtils.formatForNotification(applicationContext, amountOfTimeSecondsText, status, lapList.size)
                }.distinctUntilChanged().collect {
                    sendServiceNotification(it, stopwatchLogic.status.value)
                }
            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    private fun createServiceNotification(text: String, status: StopwatchStatus): Notification {
        val notification = NotificationCompat.Builder(applicationContext, Constants.CHANNEL_DEFAULT)
        notification.setContentTitle(applicationContext.getString(R.string.stopwatch_text))
        notification.setContentText(text)
        notification.setSmallIcon(R.drawable.ic_stopwatch_notification)
        notification.priority = NotificationCompat.PRIORITY_LOW
        notification.setOngoing(true)
        notification.setOnlyAlertOnce(true)

        when (status) {
            StopwatchStatus.RUNNING -> {
                notification.addAction(R.drawable.ic_notification_lap, application.getString(R.string.lap), lapOrResetPendingIntent)
                notification.addAction(R.drawable.ic_notification_pause, application.getString(R.string.pause), startPauseStopPendingIntent)
            }
            StopwatchStatus.CLEARED -> Unit
            StopwatchStatus.PAUSED -> {
                notification.addAction(R.drawable.ic_notification_reset, application.getString(R.string.reset), lapOrResetPendingIntent)
                notification.addAction(R.drawable.ic_notification_start, application.getString(R.string.resume), startPauseStopPendingIntent)
            }
        }

        notification.setContentIntent(activityShowPendingIntent)

        return notification.build()
    }

    private fun sendServiceNotification(text: String, status: StopwatchStatus) {
        if (notificationManager == null) return

        notificationManager?.notify(SERVICE_NOTIFICATION_ID, createServiceNotification(text, status))
    }

    override fun onDestroy() {
        super.onDestroy()

        notificationManager?.cancel(SERVICE_NOTIFICATION_ID)
    }

    companion object {
        @JvmStatic
        fun newIntent(context: Context): Intent {
            return Intent(context, StopwatchForegroundService::class.java)
        }
    }
}