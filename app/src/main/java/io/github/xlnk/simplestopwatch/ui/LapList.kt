package io.github.xlnk.simplestopwatch.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import io.github.xlnk.simplestopwatch.R
import io.github.xlnk.simplestopwatch.data.Lap
import io.github.xlnk.simplestopwatch.ui.preview.PreviewData

@Composable
fun LapList(lapList: List<Lap>, modifier: Modifier = Modifier) {
    val textStyle = MaterialTheme.typography.h6

    Column(modifier = modifier) {
        Row() {
            Text(
                text = stringResource(R.string.lap),
                modifier = Modifier.weight(1f),
                textAlign = TextAlign.Center,
                style = textStyle,
            )
            Text(
                text = stringResource(R.string.lap_times),
                modifier = Modifier.weight(1f),
                textAlign = TextAlign.Center,
                style = textStyle,
            )
            Text(
                text = stringResource(R.string.overall_time),
                modifier = Modifier.weight(1f),
                textAlign = TextAlign.Center,
                style = textStyle,
            )
        }
        Divider()
        LazyColumn(
            contentPadding = PaddingValues(vertical = 8.dp),
            reverseLayout = true,

        ) {
            itemsIndexed(items = lapList, key = { _, lap -> lap.overallTimeMs }) { index, lap ->
                LapItem(lap = lap, number = index + 1, modifier = Modifier.padding(vertical = 8.dp))
            }
        }
    }
}

@Preview
@Composable
fun LapListPreview() {
    LapList(PreviewData.lapList)
}

@Preview
@Composable
fun LapListPreviewBig() {
    LapList(PreviewData.lapListBig)
}