package io.github.xlnk.simplestopwatch.data.settings

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import io.github.xlnk.simplestopwatch.Constants
import io.github.xlnk.simplestopwatch.data.Lap
import io.github.xlnk.simplestopwatch.data.LongOperation
import io.github.xlnk.simplestopwatch.di.ApplicationCoroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingsRepositoryImpl @Inject constructor(
    @ApplicationContext
    private val context: Context,
    @ApplicationCoroutineScope
    private val externalScope: CoroutineScope,
): SettingsRepository {
    companion object {
        private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = Constants.SETTINGS_NAME)
        val PERIOD_KEY = longPreferencesKey("period")
        val LAP_LIST_STRING = stringPreferencesKey("lap_list_string")
    }

    override val settings: StateFlow<StopwatchSettings> = context.dataStore
        .data
        .map { preferences ->
            val lapListRaw = preferences[LAP_LIST_STRING]

            StopwatchSettings(
                period = preferences[PERIOD_KEY]?.takeIf { it >= 0 } ?: 0,
                loadingStatus = LongOperation.Ready,
                lapList = fromDataStoreString(lapListRaw),
            )
        }
        .catch { StopwatchSettings(loadingStatus = LongOperation.Error(it)) }
        .stateIn(externalScope, SharingStarted.WhileSubscribed(5000), StopwatchSettings())

    override fun reset() {
        externalScope.launch {
            context.dataStore.edit { preferences ->
                preferences[PERIOD_KEY] = 0
                preferences[LAP_LIST_STRING] = ""
            }
        }
    }

    override fun setPeriod(value: Long) {
        externalScope.launch {
            context.dataStore.edit { preferences ->
                preferences[PERIOD_KEY] = value
            }
        }
    }

    override fun setLapList(laps: List<Lap>) {
        externalScope.launch {
            context.dataStore.edit { preferences ->
                preferences[LAP_LIST_STRING] = toDataStoreString(laps)
            }
        }
    }

    private fun toDataStoreString(laps: List<Lap>): String {
        return laps.joinToString(separator = "|") { it.lapTimeMs.toString() }
    }

    private fun fromDataStoreString(lapListRaw: String?): List<Lap> {
        if (lapListRaw.isNullOrEmpty()) return emptyList()
        try {
            val lapTimeMsList = lapListRaw.split('|').map { it.toLong() }
            var overallTimeMs = 0L
            val result = mutableListOf<Lap>()
            for (lapTimeMs in lapTimeMsList) {
                if (lapTimeMs <= 0) {
                    return emptyList()
                }
                overallTimeMs += lapTimeMs
                result.add(Lap(lapTimeMs, overallTimeMs))
            }
            return result
        } catch (e: NumberFormatException) {
            return emptyList()
        }
    }
}