package io.github.xlnk.simplestopwatch.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.github.xlnk.simplestopwatch.data.settings.SettingsRepository
import io.github.xlnk.simplestopwatch.data.settings.SettingsRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface SettingsModule {
    @Binds
    @Singleton
    fun bindSettingRepository(impl: SettingsRepositoryImpl): SettingsRepository
}