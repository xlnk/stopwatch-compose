package io.github.xlnk.simplestopwatch.ui.preview

import io.github.xlnk.simplestopwatch.data.FormatUtil
import io.github.xlnk.simplestopwatch.data.AmountOfTimeProvider
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class PreviewAmountOfTimeProvider(_amountOfTimeMs: Long = 12345) : AmountOfTimeProvider {
    override val amountOfTimeMs: StateFlow<Long> = MutableStateFlow(_amountOfTimeMs)
    override val amountOfTimeText: StateFlow<String> = MutableStateFlow(FormatUtil.formatPeriodMs(_amountOfTimeMs))
}