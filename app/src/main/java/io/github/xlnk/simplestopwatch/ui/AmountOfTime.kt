package io.github.xlnk.simplestopwatch.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.bignerdranch.android.materia2test.ui.theme.StopwatchTheme
import io.github.xlnk.simplestopwatch.data.AmountOfTimeProvider
import io.github.xlnk.simplestopwatch.ui.preview.PreviewAmountOfTimeProvider

@Composable
fun AmountOfTime(
    amountOfTimeProvider: AmountOfTimeProvider,
    modifier: Modifier = Modifier,
) {
    val amountOfTypeText by amountOfTimeProvider.amountOfTimeText.collectAsStateWithLifecycle()

    Text(amountOfTypeText, style = MaterialTheme.typography.h3, modifier = modifier)
}

@Preview
@Composable
fun AmountOfTimePreview() {
    StopwatchTheme() {
        AmountOfTime(PreviewAmountOfTimeProvider())
    }
}