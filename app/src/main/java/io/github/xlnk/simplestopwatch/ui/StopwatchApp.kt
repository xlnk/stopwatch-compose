package io.github.xlnk.simplestopwatch.ui

import android.Manifest
import android.annotation.SuppressLint
import android.os.Build
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.bignerdranch.android.materia2test.ui.theme.StopwatchTheme
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.isGranted
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.permissions.shouldShowRationale
import io.github.xlnk.simplestopwatch.StopwatchScreen
import io.github.xlnk.simplestopwatch.data.LongOperation
import io.github.xlnk.simplestopwatch.data.StopwatchLogic


@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun StopwatchApp(
    stopwatchLogic: StopwatchLogic,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
) {
    val context = LocalContext.current
    val (permissionNotificationDialogShown, setPermissionNotificationDialogShown) = rememberSaveable { mutableStateOf(false) }

    @SuppressLint("InlinedApi")
    val permissionNotificationState = rememberPermissionState(Manifest.permission.POST_NOTIFICATIONS)

    when {
        permissionNotificationDialogShown -> Unit
        permissionNotificationState.status.shouldShowRationale -> {
            PermissionNotificationExplanationDialog(
                onRequestPermission = { permissionNotificationState.launchPermissionRequest() },
                onRejectPermission = { setPermissionNotificationDialogShown(true) })
        }
        !permissionNotificationState.status.isGranted -> {
            LaunchedEffect(Unit) {
                permissionNotificationState.launchPermissionRequest()
            }
        }
    }

    StopwatchTheme {
        Scaffold(
            scaffoldState = scaffoldState,
        ) { paddings ->
            val status by stopwatchLogic.status.collectAsStateWithLifecycle()
            val lapList by stopwatchLogic.lapList.collectAsStateWithLifecycle()
            val loadingStatusState = stopwatchLogic.loadingStatus.collectAsStateWithLifecycle()

            when (val loadingStatus = loadingStatusState.value) {
                is LongOperation.Error -> {
                    SeriousErrorScreen(error = loadingStatus.throwable)
                }
                else -> {
                    StopwatchScreen(
                        status = status,
                        onLapOrReset = stopwatchLogic::lapOrReset,
                        onStartPause = stopwatchLogic::startPause,
                        lapList = lapList,
                        modifier = Modifier.padding(paddings),
                        amountOfTimeProvider = stopwatchLogic,
                        noNotificationPermission = !permissionNotificationState.status.isGranted,
                        onOpenPermissionSettings = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) null else ({
                            UiUtils.openNotificationSettings(context)
                        }),
                        blocked = loadingStatus != LongOperation.Ready
                    )
                }
            }
        }
    }
}