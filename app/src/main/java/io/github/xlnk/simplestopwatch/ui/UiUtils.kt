package io.github.xlnk.simplestopwatch.ui

import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi
import io.github.xlnk.simplestopwatch.R
import io.github.xlnk.simplestopwatch.data.StopwatchStatus


object UiUtils {
    @RequiresApi(Build.VERSION_CODES.O)
    fun openNotificationSettings(context: Context) {
        val intent: Intent = Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .putExtra(Settings.EXTRA_APP_PACKAGE, context.packageName)

        context.startActivity(intent)
    }

    fun formatForNotification(
        context: Context,
        amountOfTimeSeconds: String,
        status: StopwatchStatus,
        lapCount: Int
    ): String = when (status) {
        StopwatchStatus.CLEARED -> ""
        StopwatchStatus.PAUSED -> context.getString(R.string.amount_of_time_pause, amountOfTimeSeconds)
        StopwatchStatus.RUNNING -> if (lapCount == 0) amountOfTimeSeconds else context.getString(R.string.amount_of_time_lap, amountOfTimeSeconds, lapCount)
    }

    fun formatError(error: Throwable): String {
        val message = error.message
        return if (message.isNullOrEmpty()) error.javaClass.simpleName else message
    }
}