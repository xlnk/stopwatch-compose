package io.github.xlnk.simplestopwatch.ui

import androidx.compose.foundation.layout.Row
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.bignerdranch.android.materia2test.ui.theme.StopwatchTheme
import io.github.xlnk.simplestopwatch.data.Lap
import io.github.xlnk.simplestopwatch.ui.preview.PreviewData

@Composable
fun LapItem(lap: Lap, number: Int, modifier: Modifier = Modifier) {
    val textStyle = MaterialTheme.typography.h6

    Row(modifier = modifier) {
        Text(
            text = number.toString(),
            modifier = Modifier.weight(1f),
            textAlign = TextAlign.Center,
            style = textStyle,
            color = MaterialTheme.colors.primaryVariant,
        )
        Text(
            text = lap.lapTimeText,
            Modifier.weight(1f),
            textAlign = TextAlign.Center,
            style = textStyle,
            color = MaterialTheme.colors.primaryVariant,
        )
        Text(
            text = lap.overallTimeText,
            Modifier.weight(1f),
            textAlign = TextAlign.Center,
            style = textStyle,
            color = MaterialTheme.colors.onSurface,
        )
    }
}

@Preview
@Composable
fun LapItemPreview() {
    StopwatchTheme() {
        LapItem(PreviewData.lap1, 1)
    }
}