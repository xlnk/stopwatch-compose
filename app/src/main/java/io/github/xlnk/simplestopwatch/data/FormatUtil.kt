package io.github.xlnk.simplestopwatch.data

object FormatUtil {
    @JvmStatic
    fun formatPeriodMs(periodMs: Long): String {
        var period = periodMs
        val milliseconds = period / 10 % 100
        period /= 1000
        val seconds = period % 60
        period /= 60
        val minutes = period % 60

        return if (period < 60) {
            "%02d:%02d.%02d".format(minutes, seconds, milliseconds)
        } else {
            val hours = period / 60
            "%d:%02d:%02d.%02d".format(hours, minutes, seconds, milliseconds)
        }
    }

    @JvmStatic
    fun formatPeriodSeconds(periodMs: Long): String {
        var period = periodMs / 1000
        val seconds = period % 60
        period /= 60
        val minutes = period % 60

        return if (period < 60) {
            "%02d:%02d".format(minutes, seconds)
        } else {
            val hours = period / 60
            "%d:%02d:%02d".format(hours, minutes, seconds)
        }
    }
}