package io.github.xlnk.simplestopwatch.ui

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Warning
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.bignerdranch.android.materia2test.ui.theme.StopwatchTheme
import io.github.xlnk.simplestopwatch.R

@Composable
fun NoNotificationPermissionWarning(onOpenPermissionSettings: (() -> Unit)?, modifier: Modifier = Modifier) {
    Card(
        border = BorderStroke(1.dp, MaterialTheme.colors.error),
        modifier = modifier,
        elevation = 4.dp,
    ) {
        Row(
            modifier = modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                stringResource(R.string.permission_notification_denied),
                style = MaterialTheme.typography.h6,
                color = MaterialTheme.colors.error,
                textAlign = TextAlign.Center,
                modifier = Modifier.weight(1f)
            )

            if (onOpenPermissionSettings != null) {
                IconButton(onClick = onOpenPermissionSettings, modifier.size(48.dp)) {
                    Icon(
                        imageVector = Icons.TwoTone.Warning,
                        contentDescription = stringResource(R.string.go_to_notification_settings),
                    )
                }
            }
        }
    }
}

@Preview
@Composable
fun NoNotificationPermissionWarningPreview() {
    StopwatchTheme() {
        NoNotificationPermissionWarning({})
    }
}