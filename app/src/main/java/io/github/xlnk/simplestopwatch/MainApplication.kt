package io.github.xlnk.simplestopwatch

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.core.content.ContextCompat
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        initChannel()
    }

    override fun onTerminate() {
        super.onTerminate()
    }

    private fun initChannel() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) return

        val notificationManager = ContextCompat.getSystemService(this, NotificationManager::class.java) ?: return

        val channel = NotificationChannel(Constants.CHANNEL_DEFAULT, this.getString(R.string.channel_default_name), NotificationManager.IMPORTANCE_LOW)
        channel.setShowBadge(false)

        notificationManager.createNotificationChannel(channel)
    }
}